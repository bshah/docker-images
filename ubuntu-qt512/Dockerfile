FROM ubuntu:bionic

LABEL Description="Base image for Qt 5.12 (LTS)"
MAINTAINER KDE Sysadmin <sysadmin@kde.org>

RUN apt-get update && apt-get install -y apt-transport-https ca-certificates gnupg software-properties-common wget

# Now install the general dependencies we need for builds
RUN apt-get install -y \
  # General requirements for building KDE software
  build-essential cmake git-core locales \
  # General requirements for building other software
  automake gcc-6 g++-6 libxml-parser-perl libpq-dev libaio-dev \
  # Needed for some frameworks
  bison gettext \
  # Qt and KDE Build Dependencies
  gperf libasound2-dev libatkmm-1.6-dev libbz2-dev libcairo-perl libcap-dev libcups2-dev libdbus-1-dev \
  libdrm-dev libegl1-mesa-dev libfontconfig1-dev libfreetype6-dev libgcrypt11-dev libgl1-mesa-dev \
  libglib-perl libgsl0-dev libgsl0-dev gstreamer1.0-alsa libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
  libgtk2-perl libjpeg-dev libnss3-dev libpci-dev libpng-dev libpulse-dev libssl-dev \
  libgstreamer-plugins-good1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base \
  gstreamer1.0-plugins-good libtiff5-dev libudev-dev libwebp-dev flex libmysqlclient-dev \
  # Mesa libraries for everything to use
  libx11-dev libxkbcommon-x11-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-util0-dev libxcb-res0-dev libxcb1-dev libxcomposite-dev libxcursor-dev \
  libxdamage-dev libxext-dev libxfixes-dev libxi-dev libxrandr-dev libxrender-dev libxss-dev libxtst-dev mesa-common-dev \
  # Kdenlive AppImage extra dependencies
  liblist-moreutils-perl libtool libpixman-1-dev subversion

# Setup a user account for everything else to be done under
RUN useradd -d /home/kde/ -u 1000 --user-group --create-home -G video kde

# Get locales in order
RUN locale-gen en_US en_US.UTF-8

# Switch to kde user
USER kde
